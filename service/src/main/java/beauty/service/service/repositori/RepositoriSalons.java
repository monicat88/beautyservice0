package beauty.service.service.repositori;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import beauty.service.service.repositori.Entitats.Salons;

@Repository
public class RepositoriSalons {

    @Autowired
    private CrudRepositoriSalons crudRepositoriSalons;

    public Salons getSaloByCif(String cif) {
        Iterable<Salons> allSalons = crudRepositoriSalons.findAll();
        for(Salons s: allSalons) {
            if (s.getCifSalo().equals(cif)) {
                return s;
            }
        }
        return null;
    }
}

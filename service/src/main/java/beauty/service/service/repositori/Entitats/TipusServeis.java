package beauty.service.service.repositori.Entitats;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="TipusServeis")
public class TipusServeis implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "idTipusServei")
    @Size(max=11)
    private int idTipusServei;

    @NotNull
    @Column(name = "nomTipusServei")
    @Size(max=45)
    private String nomTipusServei;

    @Column(name = "descripcioTipus")
    @Size(max=120)
    private String descripcioTipus;

    //Getters i Setters
    public int getIdTipusServei() {
        return idTipusServei;
    }

    public void setIdTipusServei(int idTipusServei) {
        this.idTipusServei = idTipusServei;
    }

    public String getNomTipusServei() {
        return nomTipusServei;
    }

    public void setNomTipusServei(String nomTipusServei) {
        this.nomTipusServei = nomTipusServei;
    }

    public String getDescripcioTipus() {
        return descripcioTipus;
    }

    public void setDescripcioTipus(String descripcioTipus) {
        this.descripcioTipus = descripcioTipus;
    }

    
}

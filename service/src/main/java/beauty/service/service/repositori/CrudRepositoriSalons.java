package beauty.service.service.repositori;

import org.springframework.data.repository.CrudRepository;

import beauty.service.service.repositori.Entitats.Salons;

//Interfície que crida a CrudRepository
public interface CrudRepositoriSalons extends CrudRepository<Salons, Integer> {

}
package beauty.service.service.repositori.Entitats;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Usuari implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Size(max=9)
    private int id;

    @NotNull
    @Column(name = "nom")
    @Size(max=20)
    private String nom;

    @NotNull
    @Column(name = "cognomsUsuari")
    @Size(max=45)
    private String cognoms;

    @Column(name = "telefon")
    @Size(max=9)
    private String telefon;

    @Column(name="mail")
    @Size(max=45)
    @NotNull
    private String mail;

    @NotNull
    @Column(name = "nomUsuari")
    @Size(max=20)
    private String nomUsuari;

    @Column(name="contrasenya")
    @Size(max=45)
    @NotNull
    private String contrasenya;


    @Column(name="ciutat")
    @Size(max=45)
    @NotNull
    private String ciutat;

    @Column(name="codiPostal")
    @Size(max=5)
    @NotNull
    private String codiPostal;

    //Getters i Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomUsuari() {
        return nomUsuari;
    }

    public void setNomUsuari(String nomUsuari) {
        this.nomUsuari = nomUsuari;
    }

    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCognoms() {
        return cognoms;
    }

    public void setCognoms(String cognoms) {
        this.cognoms = cognoms;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getContrasenya() {
        return contrasenya;
    }

    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
    }

    public String getCiutat() {
        return ciutat;
    }

    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    public String getCodiPostal() {
        return codiPostal;
    }

    public void setCodiPostal(String codiPostal) {
        this.codiPostal = codiPostal;
    }


    
}

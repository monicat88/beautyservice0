package beauty.service.service.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import beauty.service.service.repositori.CrudRepositoriSalons;
import beauty.service.service.repositori.RepositoriSalons;
import beauty.service.service.repositori.Entitats.Salons;

@Service
@Transactional
public class SalonsService {
    
    @Autowired
    private CrudRepositoriSalons repositoriCrud;
    @Autowired
    private RepositoriSalons repositori;

    //Mètode que retorna tots els salons 
    public Iterable<Salons> getAllSalons() {
        return repositoriCrud.findAll();
    }
    
    //Mètode que guarda un saló a la BD
    public void save(Salons salo) {
        repositoriCrud.save(salo);
    }    
 
    //Mètode que retorna un saló pel seu cif
    public Salons getSaloByCif(String cifSalo) {
        return repositori.getSaloByCif(cifSalo);
    }
}

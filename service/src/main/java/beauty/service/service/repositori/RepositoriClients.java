package beauty.service.service.repositori;

import beauty.service.service.repositori.Entitats.Clients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RepositoriClients {

    @Autowired
    private CrudRepositoriClients crudRepositoriClients;

    public Clients getClientByNif(String dni) {
        Iterable<Clients> allClients = crudRepositoriClients.findAll();
        for(Clients c: allClients) {
            if (c.getNifClient().equals(dni)) {
                return c;
            }
        }
        return null;
    }
}

package beauty.service.service.repositori;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import beauty.service.service.repositori.Entitats.Usuari;

@Repository
public class RepositoriUsuaris {
    @Autowired
    private CrudRepositoriUsuari crudRepositori;

    public int getCodi() {
        int comptador=0;
        Iterable<Usuari> allUsuaris = crudRepositori.findAll();
        for (Usuari u: allUsuaris) {
            comptador++;
        }
        return comptador;
    }
}
